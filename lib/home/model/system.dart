class System {
   int id;
   String name;

   System({this.id, this.name});

   factory System.fromJson(Map<String, dynamic> json) {
     return System(
         id: json['id'] as int,
         name: json['name'] as String
     );
   }
}
class Beacon {
  int id;
  String uuid;
  String typeName;
  String systemId;
  String state;
  int ttx;

  Beacon(
      {this.id, this.uuid, this.typeName, this.systemId, this.state, this.ttx});

  factory Beacon.fromJson(Map<String, dynamic> json) {
    return Beacon(
      id: json['id'] as int,
      uuid: json['uuid'] as String,
      typeName: json['typeName'] as String,
      systemId: json['systemId'] as String,
      state: json['state'] as String,
      ttx: json['ttx'] as int,
    );
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["uuid"] = uuid;
    map["typeName"] = typeName;
    map["systemId"] = systemId;
    map["state"] = state;
    map["ttx"] = ttx;

    return map;
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:deep_market_data_clear/auth/bloc/auth_bloc.dart';
import 'package:deep_market_data_clear/auth/bloc/auth_event.dart';
import 'package:deep_market_data_clear/auth/bloc/auth_state.dart';
import 'package:deep_market_data_clear/auth/model/user.dart';
import 'package:deep_market_data_clear/home/bloc/home_bloc.dart';
import 'package:deep_market_data_clear/home/bloc/home_event.dart';
import 'package:deep_market_data_clear/home/bloc/home_state.dart';
import 'package:deep_market_data_clear/home/model/beacon.dart';
import 'package:deep_market_data_clear/home/model/system.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  AuthBloc _authBloc;
  HomeBloc _homeBloc;
  User _user;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(context);
    _homeBloc = HomeBloc();
    _user = _authBloc.currentState.user;
    _homeBloc.dispatch(SystemEvent(token: _authBloc.currentState.token));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _authBloc,
      listener: (context, AuthState authState) {
        if (authState.error != null) {
          showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: Text('ERROR', textAlign: TextAlign.center),
                  content: Text(authState.error),
                  actions: <Widget>[
                    RaisedButton(
                      onPressed: () => Navigator.pop(context),
                      color: Colors.purple,
                      child: Text(
                        'Ok',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                );
              });
        }
      },
      child: BlocBuilder(
        bloc: _homeBloc,
        builder: (BuildContext context, HomeState state) {
          if (state.isLoading) {
            return Scaffold(
              appBar: AppBar(
                title: Text("Beacon's Managment"),
              ),
              body: Center(child: CircularProgressIndicator()),
            );
          } else {
            return Scaffold(
              appBar: AppBar(
                title: Text("Beacon's Managment"),
                actions: <Widget>[
                  if (state.isBeaconsLoading)
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Center(
                          child: SizedBox(
                              height: 32.0,
                              width: 32.0,
                              child: CircularProgressIndicator())),
                    ),
                ],
              ),
              drawer: Drawer(
                child: Column(
                  children: <Widget>[
                    UserAccountsDrawerHeader(
                      accountEmail:
                          Text(_user.email ?? "Email is not available"),
                      accountName: Text(_user.username),
                      currentAccountPicture: CachedNetworkImage(
                        imageUrl: _user.iconUrl ?? '',
                        errorWidget: (context, s, o) =>
                            Icon(Icons.broken_image),
                        placeholder: (context, s) => Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    ),
                    if (state.systems?.isNotEmpty ?? false)
                      ...state.systems.map((System _system) {
                        return Column(
                          children: <Widget>[
                            ListTile(
                              leading: Icon(Icons.adjust),
                              title: Text(_system.name),
                              onTap: () {
                                _homeBloc.dispatch(BeaconEvent(
                                  id: _system.id.toString(),
                                  token: _authBloc.currentState.token,
                                ));
                              },
                            ),
                            Divider(),
                          ],
                        );
                      }).toList(),
                    ListTile(
                      leading: Icon(Icons.exit_to_app),
                      title: Text('Logout'),
                      onTap: () {
                        Navigator.popUntil(
                            context, (_) => !Navigator.canPop(context));
                        _authBloc.dispatch(Logout());
                      },
                    ),
                  ],
                ),
              ),
              body: Column(
                mainAxisAlignment: state.beacons.isEmpty
                    ? MainAxisAlignment.center
                    : MainAxisAlignment.start,
                children: <Widget>[
                  if (state.beacons.isNotEmpty)
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 8.0),
                          child: Text("Beacon's list"),
                        ),
                        Divider(),
                      ],
                    ),
                  if (state.beacons.isEmpty)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Icon(Icons.ac_unit),
                        ),
                        Text('No Beacons available',
                            textAlign: TextAlign.center),
                      ],
                    ),
                  if (state.beacons.isNotEmpty)
                    Expanded(
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: state.beacons.length ?? 0,
                        itemBuilder: (BuildContext context, int index) {
                          Beacon _beacon = state.beacons[index];

                          return BeaconCard(
                            uuid: _beacon.uuid,
                            ttx: _beacon.ttx.toString(),
                            state: _beacon.state,
                            onChanged: (String state) {
                              _homeBloc.dispatch(BeaconSwitchEvent(
                                id: _beacon.id,
                                token: _authBloc.currentState.token,
                                state: state,
                              ));
                            },
                          );
                        },
                      ),
                    ),
                ],
              ),
            );
          }
        },
      ),
    );
  }
}

class BeaconCard extends StatelessWidget {
  final String uuid;
  final String ttx;
  final String state;
  final Function(String state) onChanged;

  const BeaconCard({Key key, this.uuid, this.ttx, this.state, this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('UUID:' + this.uuid),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Switch(
                activeColor: Colors.purple[300],
                value: state == 'ACTIVE' || false,
                onChanged: (bool state) {
                  if (state)
                    onChanged('ACTIVE');
                  else
                    onChanged('DISABLED');
                },
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('TTX: ' + this.ttx),
                Text('State: ' + this.state),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

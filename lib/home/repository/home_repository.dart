import 'dart:convert';

import 'package:deep_market_data_clear/constant/url_constants.dart';
import 'package:deep_market_data_clear/home/bloc/home_event.dart';
import 'package:deep_market_data_clear/home/model/beacon.dart';
import 'package:deep_market_data_clear/home/model/system.dart';
import 'package:http/http.dart';

class HomeRepository {
  Future<List<System>> getAllSystems(String token) async {
    final response = await get(UrlConstants.getAllSystemsUrl,
        headers: {'Authorization': 'Bearer ' + token}).catchError((e) {
      print(e);
    });

    if (response == null) {
      throw ("No connection");
    } else {
      if (response.statusCode == 200) {
        List<dynamic> systemsMaps = jsonDecode(response.body);
        List<System> systems =
            systemsMaps.map((i) => System.fromJson(i)).toList();

        return systems;
      } else {
        throw (AuthError.fromMap(jsonDecode(response.body)).message);
      }
    }
  }

  Future<List<Beacon>> getAllBeacons(String id, String token) async {
    final response = await get(UrlConstants.getAllBeaconsUrl + '/' + id,
        headers: {'Authorization': 'Bearer ' + token}).catchError((e) {});

    if (response == null) {
      throw ("No connection");
    } else {
      if (response.statusCode == 200) {
        List<dynamic> res = jsonDecode(response.body);
        List<Beacon> beaconList = res.map((i) => Beacon.fromJson(i)).toList();

        return beaconList;
      } else {
        throw (AuthError.fromMap(jsonDecode(response.body)).message);
      }
    }
  }

  Future<bool> setBeaconState(int id, String state, String token) async {
    final response = await put(UrlConstants.setBeaconStateUrl,
        body: jsonEncode(Beacon(id: id, state: state).toMap()),
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': "application/json"
        }).catchError((e) {});

    if (response == null) {
      throw ("No connection");
    } else {
      if (response.statusCode == 200) {
        return true;
      } else {
        throw (AuthError.fromMap(jsonDecode(response.body)).message);
      }
    }
  }
}

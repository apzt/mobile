import 'package:bloc/bloc.dart';
import 'package:deep_market_data_clear/home/model/beacon.dart';
import 'package:deep_market_data_clear/home/model/system.dart';
import 'package:deep_market_data_clear/home/repository/home_repository.dart';

import 'home_event.dart';
import 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final HomeRepository _homeRepository = HomeRepository();

  @override
  HomeState get initialState => HomeState(
        beacons: List<Beacon>(),
        systems: List<System>(),
        isLoading: false,
        isBeaconsLoading: false,
      );

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is BeaconEvent) {
      yield* _beaconLoad(event);
    } else if (event is SystemEvent) {
      yield* _systemsLoad(event);
    } else if (event is BeaconSwitchEvent) {
      yield* _changeBeaconState(event);
    } else if (event is AuthError) {
      yield HomeFailureState(event.message.toString());
    }
  }

  Stream<HomeState> _beaconLoad(BeaconEvent event) async* {
    try {
      yield currentState.copyWith(isBeaconsLoading: true);
      List<Beacon> beacons = await _homeRepository
          .getAllBeacons(event.id, event.token)
          .catchError((e) {
        dispatch(AuthError(message: e));
      });
      yield currentState.copyWith(
        systems: currentState.systems,
        beacons: beacons,
        isBeaconsLoading: false,
      );
    } on Exception catch (error) {
      yield HomeFailureState(error.toString().toString());
    }
  }

  Stream<HomeState> _systemsLoad(SystemEvent event) async* {
    try {
      yield currentState.copyWith(isBeaconsLoading: true);
      List<System> systems =
          await _homeRepository.getAllSystems(event.token).catchError((e) {
        dispatch(AuthError(message: e));
      });
      yield currentState.copyWith(
        beacons: currentState.beacons,
        systems: systems,
        isLoading: false,
        isBeaconsLoading: false,
      );
    } on Exception catch (error) {
      yield HomeFailureState(error.toString().toString());
    }
  }

  Stream<HomeState> _changeBeaconState(BeaconSwitchEvent event) async* {
    try {
      bool res = await _homeRepository.setBeaconState(
          event.id, event.state, event.token);
      if (res) {
        List<Beacon> stateBeacons = currentState.beacons.toList();
        Beacon updatedBeacon = currentState.beacons.firstWhere((Beacon beacon) {
          return beacon.id == event.id;
        }, orElse: () => null);
        updatedBeacon.state = event.state;
        stateBeacons
            .removeWhere((Beacon beacon) => beacon.id == updatedBeacon.id);
        stateBeacons.add(updatedBeacon);
        yield currentState.copyWith(beacons: stateBeacons);
      }
    } on Exception catch (error) {
      yield HomeFailureState(error.toString().toString());
    }
  }
}

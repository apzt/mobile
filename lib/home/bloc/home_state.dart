import 'package:deep_market_data_clear/home/model/beacon.dart';
import 'package:deep_market_data_clear/home/model/system.dart';

class HomeState {
  final List<Beacon> beacons;
  final List<System> systems;
  final bool isLoading;
  final bool isBeaconsLoading;

  HomeState({
    this.beacons,
    this.systems,
    this.isLoading = false,
    this.isBeaconsLoading = false,
  });

  HomeState copyWith({
    List<Beacon> beacons,
    List<System> systems,
    bool isLoading,
    bool isBeaconsLoading,
  }) {
    return HomeState(
      isLoading: isLoading ?? this.isLoading,
      isBeaconsLoading: isBeaconsLoading ?? this.isBeaconsLoading,
      beacons: beacons ?? this.beacons,
      systems: systems ?? this.systems,
    );
  }
}

class HomeFailureState extends HomeState {
  final String error;

  HomeFailureState(this.error);

  @override
  String toString() => 'Home State Failure! $error';
}

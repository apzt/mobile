abstract class HomeEvent {}

class SystemEvent extends HomeEvent {
  final String token;

  SystemEvent({this.token});
}

class BeaconEvent extends HomeEvent {
  final String id;
  final String token;

  BeaconEvent({this.token, this.id});
}

class BeaconSwitchEvent extends HomeEvent {
  final int id;
  final String state;
  final String token;

  BeaconSwitchEvent({this.id, this.state, this.token});
}

class AuthError extends HomeEvent {
  final int status;
  final String timestamp;
  final String message;
  final String debugMessage;

  AuthError({this.status, this.timestamp, this.message, this.debugMessage});

  factory AuthError.fromMap(Map<String, dynamic> map) {
    return AuthError(
        status: map['status'] as int,
        timestamp: map['timestamp'] as String,
        message: map['message'] as String,
        debugMessage: map['debugMessage'] as String);
  }
}

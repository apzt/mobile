class UrlConstants {
  static const String loginUser = 'https://deepmarketdata.herokuapp.com/auth/login';
  static const String getUserUrl = 'https://deepmarketdata.herokuapp.com/profile';
  static const String getAllSystemsUrl = 'https://deepmarketdata.herokuapp.com/systems/current';
  static const String getAllBeaconsUrl = 'https://deepmarketdata.herokuapp.com/settings/beacons/system'; // ...settings/beacons/system/1 - system number
  static const String setBeaconStateUrl = 'https://deepmarketdata.herokuapp.com/settings/beacon/state';
}

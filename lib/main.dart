import 'package:deep_market_data_clear/auth/bloc/auth_bloc.dart';
import 'package:deep_market_data_clear/home/screen/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'auth/bloc/auth_state.dart';
import 'auth/screen/sign_in.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final authBloc = AuthBloc();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: authBloc,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            brightness: Brightness.dark,
            primarySwatch: Colors.purple,
            accentColor: Colors.white),
        home: BlocBuilder(
          bloc: authBloc,
          builder: (context, AuthState authState) {
            if (authState.isLoading) {
              return Scaffold(
                body: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
            if (authState.user == null) {
              return SignInScreen();
            }
            return HomeScreen();
          },
        ),
      ),
    );
  }
}

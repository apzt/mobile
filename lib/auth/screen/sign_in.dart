import 'package:cached_network_image/cached_network_image.dart';
import 'package:deep_market_data_clear/auth/bloc/auth_bloc.dart';
import 'package:deep_market_data_clear/auth/bloc/auth_event.dart';
import 'package:deep_market_data_clear/auth/bloc/auth_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final nameController = TextEditingController();
  final passController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  bool _isObscured;
  AuthBloc _authBloc;

  @override
  void initState() {
    super.initState();
    nameController.text = 'admin2345';
    passController.text = 'string';
    _isObscured = true;
    _authBloc = BlocProvider.of<AuthBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _authBloc,
      listener: (context, AuthState authState) {
        if (authState.error != null) {
          showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: Text(
                    'ERROR',
                    textAlign: TextAlign.center,
                  ),
                  content: Text(authState.error),
                  actions: <Widget>[
                    RaisedButton(
                      onPressed: () => Navigator.pop(context),
                      color: Colors.purple,
                      child: Text(
                        'Ok',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                );
              });
        }
      },
      child: Scaffold(
          appBar: AppBar(
            title: Text('Deep market data'),
            centerTitle: true,
          ),
          body: Form(
            key: formKey,
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 12.0, vertical: 8.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: CachedNetworkImage(
                      imageUrl:
                          'https://www.utreee.com/wp-content/uploads/2019/04/Data-Analysis-1080x675.jpg',
                      errorWidget: (context, s, o) => Icon(Icons.broken_image),
                      placeholder: (context, s) => Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 12.0, vertical: 2.0),
                  child: Text(
                    'Login:',
                    style: TextStyle(fontSize: 18.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: nameController,
                    validator: emailValidator,
                    inputFormatters: [LengthLimitingTextInputFormatter(19)],
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12.0),
                        ),
                        prefixIcon: Icon(Icons.person),
                        hintText: 'Enter your login'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 12.0, vertical: 2.0),
                  child: Text(
                    'Password:',
                    style: TextStyle(fontSize: 18.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: passController,
                    validator: passwordValidator,
                    obscureText: _isObscured,
                    inputFormatters: [LengthLimitingTextInputFormatter(19)],
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12.0),
                        ),
                        prefixIcon: Icon(Icons.security),
                        hintText: 'Enter your password',
                        suffixIcon: GestureDetector(
                          onTap: () {
                            setState(() {
                              _isObscured = !_isObscured;
                            });
                          },
                          child: Icon(
                            Icons.remove_red_eye,
                            color:
                                _isObscured ? Colors.white : Colors.purple[300],
                          ),
                        )),
                  ),
                ),
                _button(
                    text: 'Sign In',
                    onPressed: () {
                      if (formKey.currentState.validate()) {
                        _authBloc.dispatch(SignIn(
                          password: passController.text,
                          username: nameController.text,
                        ));
                      }
                    }),
//                _button(
//                    text: 'Sign In with Google',
//                    color: Colors.red,
//                    onPressed: () {
//                      print('adssda');
//                    })
              ],
            ),
          )),
    );
  }

  Widget _button({String text, VoidCallback onPressed, Color color}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 4.0),
      child: RaisedButton(
        color: color,
        onPressed: onPressed,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        child: Text(text, style: TextStyle(color: Colors.white)),
      ),
    );
  }

  String emailValidator(String value) {
    if (value.trim().isEmpty) {
      return 'Please complete this field';
    }
    return null;
  }

  String passwordValidator(String value) {
    if (value.trim().isEmpty) {
      return 'Please complete this field';
    }
    return null;
  }
}


abstract class AuthEvent{

}

class SignIn extends AuthEvent {
  final String username;
  final String password;

  SignIn({this.username, this.password});
}

class Logout extends AuthEvent {

}

class AuthError extends AuthEvent {
  final int status;
  final String timestamp;
  final String message;
  final String debugMessage;

  AuthError({this.status, this.timestamp, this.message, this.debugMessage});

  factory AuthError.fromMap(Map<String, dynamic> map) {
    return AuthError(
      status: map['status'] as int,
      timestamp: map['timestamp'] as String,
      message: map['message'] as String,
      debugMessage: map['debugMessage'] as String
    );
  }
}
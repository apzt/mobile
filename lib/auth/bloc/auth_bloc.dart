import 'package:deep_market_data_clear/auth/model/user.dart';
import 'package:deep_market_data_clear/auth/repository/auth_repository.dart';

import 'auth_event.dart';
import 'auth_state.dart';
import 'package:bloc/bloc.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository _authRepository = AuthRepository();

  @override
  AuthState get initialState => AuthState();

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is SignIn) {
      yield currentState.copyWith(isLoading: true);
      var token = await login(event.username, event.password);
        yield currentState.copyWith(token: token, isLoading: false);
      User user = await _authRepository.getUserProfile(token).catchError((e) {
        dispatch(AuthError(message: e));
      });
      if (user != null) {
        yield currentState.copyWith(user: user, isLoading: false);
      }
    } else if (event is Logout) {
      yield AuthState();
    } else if (event is AuthError) {
      yield currentState.copyWith(isLoading: false, error: event.message);
    }
  }

  Future<String> login(String username, String password) async {
    final String token = await _authRepository
        .signIn(username: username, password: password)
        .catchError((e) {
      dispatch(AuthError(message: e));
    });
    if (token != null) {
      return token;
    }
  }

//  Stream<AuthState> generateUserStream(String username, String password) async* {
//    yield currentState.copyWith(isLoading: true);
//    var token = await login(username, password);
//    currentState.copyWith(token: token, isLoading: false);
//    final User user = await _authRepository.getUserProfile(token).catchError((e) {
//      dispatch(AuthError(message: e));
//    });
//    if (user != null) {
//      yield currentState.copyWith(user: user, isLoading: false);
//    }
//  }
}

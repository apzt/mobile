import 'package:deep_market_data_clear/auth/model/user.dart';

class AuthState {
  final User user;
  final String token;
  final bool isLoading;
  final String error;

  AuthState({this.token, this.user, this.isLoading = false, this.error});

  AuthState copyWith({String token, User user, bool isLoading, String error}) {
    return AuthState(
      token: token ?? this.token,
      user: user ?? this.user,
      isLoading: isLoading ?? this.isLoading,
      error: error ?? this.error
    );

  }
}
import 'dart:convert';

import 'package:deep_market_data_clear/auth/bloc/auth_event.dart';
import 'package:deep_market_data_clear/auth/model/auth_request.dart';
import 'package:deep_market_data_clear/auth/model/auth_response.dart';
import 'package:deep_market_data_clear/auth/model/user.dart';
import 'package:deep_market_data_clear/constant/url_constants.dart';
import 'package:http/http.dart';

class AuthRepository {
  Future<String> signIn({String username, String password}) async {
    final response = await post(UrlConstants.loginUser,
        body: jsonEncode(AuthRequestDTO(username, password).toMap()),
        headers: {'Content-Type': "application/json"}).catchError((e) {});
    if (response == null) {
      throw ("No connection");
    } else {
      if (response.statusCode == 200) {
        return AuthResponse.fromMap(jsonDecode(response.body)).accessToken;
      } else {
        throw (AuthError.fromMap(jsonDecode(response.body)).message);
      }
    }
  }

  Future<User> getUserProfile(String token) async {
    final userResponse = await get(UrlConstants.getUserUrl,
            headers: {'Authorization': 'Bearer ' + token})
        .catchError((e) {});
    if (userResponse == null) {
      throw ("No Connection");
    } else {
      if (userResponse.statusCode == 200) {
        return User.fromJson(jsonDecode(userResponse.body));
      } else {
        throw (userResponse.statusCode);
      }
    }
  }
}

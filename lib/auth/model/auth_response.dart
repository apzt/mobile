class AuthResponse {
  final String accessToken;
  final String tokenType;

  AuthResponse({this.accessToken, this.tokenType});

  AuthResponse copyWith({
    String accessToken,
    String tokenType,
  }) {
    return AuthResponse(
      accessToken: accessToken ?? this.accessToken,
      tokenType: tokenType ?? this.tokenType,
    );
  }

  factory AuthResponse.fromMap(Map<String, dynamic> map) {
    return AuthResponse(
      accessToken: map['accessToken'] as String,
      tokenType: map['tokenType'] as String,
    );
  }
}

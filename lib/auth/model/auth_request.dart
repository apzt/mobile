class AuthRequestDTO {
  String username;
  String password;

  AuthRequestDTO(this.username, this.password);

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = username;
    map["password"] = password;

    return map;
  }

}

class User {
  final int id;
  final String username;
  final String email;
  final String iconUrl;

  User({this.id, this.username, this.email, this.iconUrl});

  User copyWith({int id,String username,String email,String iconUrl}){
    return User(
      username: username ?? this.username,
      email: email ?? this.email,
      iconUrl: iconUrl ?? this.iconUrl,
      id: id ?? this.id,
    );
}



  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      username: json['username'],
      email: json['email'],
      iconUrl: json['iconUrl'],
    );
  }
}